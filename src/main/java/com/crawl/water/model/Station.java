package com.crawl.water.model;

public class Station {
	private int id;
	private String code;
	private String OriginalName;
	private String TranslateName;
	private int AreaId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getOriginalName() {
		return OriginalName;
	}
	public void setOriginalName(String originalName) {
		OriginalName = originalName;
	}
	public String getTranslateName() {
		return TranslateName;
	}
	public void setTranslateName(String translateName) {
		TranslateName = translateName;
	}
	public int getAreaId() {
		return AreaId;
	}
	public void setAreaId(int areaId) {
		AreaId = areaId;
	}
	
}
