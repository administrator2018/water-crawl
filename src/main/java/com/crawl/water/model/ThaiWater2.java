package com.crawl.water.model;

public class ThaiWater2 {
	private String basin;
	private String order_basin;
	private String code;
	private String name;
	private String tambon_name;
	private String province_name;
	private String date;
	private String time;
	private String rain_yesterday;
	private String raintoday;
	private String wl_present;
	private String wl_present_adjust;
	private String wl_past;
	private String wl_past_adjust;
	private String wl_trend;
	private String ground_level;
	private String status;
	private String left_bank;
	private String right_bank;
	private String rid_critical_level;
	private String lat;
	private String lng;
	private String bank;
	private String capacity;
	private String wl_offset;
	private String station_type;
	private String share_data;
	private String amphoe_name;
	
	public String getAmphoe_name() {
		return amphoe_name;
	}
	public void setAmphoe_name(String amphoe_name) {
		this.amphoe_name = amphoe_name;
	}
	public String getBasin() {
		return basin;
	}
	public void setBasin(String basin) {
		this.basin = basin;
	}
	public String getOrder_basin() {
		return order_basin;
	}
	public void setOrder_basin(String order_basin) {
		this.order_basin = order_basin;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTambon_name() {
		return tambon_name;
	}
	public void setTambon_name(String tambon_name) {
		this.tambon_name = tambon_name;
	}
	public String getProvince_name() {
		return province_name;
	}
	public void setProvince_name(String province_name) {
		this.province_name = province_name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getRain_yesterday() {
		return rain_yesterday;
	}
	public void setRain_yesterday(String rain_yesterday) {
		this.rain_yesterday = rain_yesterday;
	}
	public String getRaintoday() {
		return raintoday;
	}
	public void setRaintoday(String raintoday) {
		this.raintoday = raintoday;
	}
	public String getWl_present() {
		return wl_present;
	}
	public void setWl_present(String wl_present) {
		this.wl_present = wl_present;
	}
	public String getWl_present_adjust() {
		return wl_present_adjust;
	}
	public void setWl_present_adjust(String wl_present_adjust) {
		this.wl_present_adjust = wl_present_adjust;
	}
	public String getWl_past() {
		return wl_past;
	}
	public void setWl_past(String wl_past) {
		this.wl_past = wl_past;
	}
	public String getWl_past_adjust() {
		return wl_past_adjust;
	}
	public void setWl_past_adjust(String wl_past_adjust) {
		this.wl_past_adjust = wl_past_adjust;
	}
	public String getWl_trend() {
		return wl_trend;
	}
	public void setWl_trend(String wl_trend) {
		this.wl_trend = wl_trend;
	}
	public String getGround_level() {
		return ground_level;
	}
	public void setGround_level(String ground_level) {
		this.ground_level = ground_level;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLeft_bank() {
		return left_bank;
	}
	public void setLeft_bank(String left_bank) {
		this.left_bank = left_bank;
	}
	public String getRight_bank() {
		return right_bank;
	}
	public void setRight_bank(String right_bank) {
		this.right_bank = right_bank;
	}
	public String getRid_critical_level() {
		return rid_critical_level;
	}
	public void setRid_critical_level(String rid_critical_level) {
		this.rid_critical_level = rid_critical_level;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getWl_offset() {
		return wl_offset;
	}
	public void setWl_offset(String wl_offset) {
		this.wl_offset = wl_offset;
	}
	public String getStation_type() {
		return station_type;
	}
	public void setStation_type(String station_type) {
		this.station_type = station_type;
	}
	public String getShare_data() {
		return share_data;
	}
	public void setShare_data(String share_data) {
		this.share_data = share_data;
	}
	

	
}
