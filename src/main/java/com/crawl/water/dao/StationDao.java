package com.crawl.water.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.crawl.water.model.Station;

public class StationDao {
	private static final Logger logger = LogManager.getLogger(StationDao.class);

	public void save(Station station) {
		if (station.getOriginalName() == null || "".equals(station.getOriginalName()) == true || existed(station) == true)
			return;
		String sql = "INSERT INTO station(OriginalName, AreaId) " + "VALUES (?,?)";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setString(1, station.getOriginalName());
			preStm.setInt(2, station.getAreaId());
			preStm.execute();
		} catch (SQLException ex) {
			logger.error(ex);
		}

	}
	public int getIdByName(String name) {
		int id = 0;
		String sql = "select id  from station where lower(originalName)=?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (name == null ? "" : name).toLowerCase());
			ResultSet rs = preStm.executeQuery();

			if (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException ex) {
			logger.error(ex);
		}
		return id;
	}	
/*	public void save(List<Category> categories) {
		for (Category category : categories) {
			save(category);
		}
	}*/

	public boolean existed(Station station) {
		boolean existed = false;
		String sql = "select count(*) as total from station where lower(originalName)=?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (station.getOriginalName() == null ? "" : station.getOriginalName()).toLowerCase());
			ResultSet rs = preStm.executeQuery();
			int total = 0;
			if (rs.next()) {
				total = rs.getInt(1);
			}
			if (total > 0) {
				existed = true;
			}
		} catch (SQLException ex) {
			logger.error(ex);
		}
		return existed;
	}

}
