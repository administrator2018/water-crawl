package com.crawl.water.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.crawl.water.model.Area;

public class AreaDao {
	private static final Logger logger = LogManager.getLogger(AreaDao.class);

	public void save(Area area) {
		if (area.getOriginalName() == null || "".equals(area.getOriginalName()) == true || existed(area) == true)
			return;
		String sql = "INSERT INTO area(originalName) " + "VALUES (?)";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setString(1, area.getOriginalName());
			preStm.execute();
		} catch (SQLException ex) {
			logger.error(ex);
		}

	}

/*	public void save(List<Category> categories) {
		for (Category category : categories) {
			save(category);
		}
	}*/

	public int getIdByName(String name) {
		int id = 0;
		String sql = "select id  from area where lower(originalName)=?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (name == null ? "" : name).toLowerCase());
			ResultSet rs = preStm.executeQuery();

			if (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException ex) {
			logger.error(ex);
		}
		return id;
	}
	
	public boolean existed(Area area) {
		boolean existed = false;
		String sql = "select count(*) as total from area where lower(originalName)=?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (area.getOriginalName() == null ? "" : area.getOriginalName()).toLowerCase());
			ResultSet rs = preStm.executeQuery();
			int total = 0;
			if (rs.next()) {
				total = rs.getInt(1);
			}
			if (total > 0) {
				existed = true;
			}
		} catch (SQLException ex) {
			logger.error(ex);
		}
		return existed;
	}

}
