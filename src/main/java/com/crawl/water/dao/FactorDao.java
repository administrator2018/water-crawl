package com.crawl.water.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.crawl.water.model.Factor;

public class FactorDao {
	private static final Logger logger = LogManager.getLogger(FactorDao.class);

	public void save(Factor factor) {

		String sql = "INSERT INTO factor(name,date, time, value, unit,stationId) " + "VALUES (?,?,?,?,?,?)";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setString(1, factor.getName());
			preStm.setString(2, factor.getDate());
			preStm.setString(3, factor.getTime());
			preStm.setString(5, factor.getUnit());
			preStm.setString(4, factor.getValue());
			preStm.setInt(6, factor.getStationId());
			
			preStm.execute();
		} catch (SQLException ex) {
			logger.error(ex);
		}

	}
	



/*	public void save(List<Category> categories) {
		for (Category category : categories) {
			save(category);
		}
	}
*/
/*	public boolean existed(Factor factor) {
		boolean existed = false;
		String sql = "select count(*) as total from category where lower(name)=?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (category.getName() == null ? "" : category.getName()).toLowerCase());
			ResultSet rs = preStm.executeQuery();
			int total = 0;
			if (rs.next()) {
				total = rs.getInt(1);
			}
			if (total > 0) {
				existed = true;
			}
		} catch (SQLException ex) {
			logger.error(ex);
		}
		return existed;
	}
*/
}
