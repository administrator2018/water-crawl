package com.crawl.water.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommonDao {
	private static final Logger logger = LogManager.getLogger(CommonDao.class);

	public static void clearDb() {
		try (Connection con = DataSource.getConnection(); Statement stm = con.createStatement();) {
			stm.execute("truncate area");
			stm.execute("truncate station");
			stm.execute("truncate factor");
		} catch (SQLException ex) {
			logger.error(ex);
		}
	}
}
