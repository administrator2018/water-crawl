package com.crawl.water.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.crawl.water.common.Constants;
import com.crawl.water.model.Factor;
import com.google.gson.Gson;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

public class ThaiWaterPage3 implements PageProcessor {
	private Site site = Site.me().setCycleRetryTimes(3).setRetryTimes(3).setSleepTime(500).setTimeOut(1 * 60 * 1000)
			.setUserAgent(Constants.USER_AGENT)
			// .addHeader("Accept",
			// "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
			// .addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
			.setCharset("UTF-8");
	private String ROOT_URL = "http://www.thaiwater.net";
	private boolean isDev = true;
	private static final Logger logger = LogManager.getLogger(ThaiWaterPage3.class);

	@Override
	public Site getSite() {
		return site;
	}

	public static void main(String[] args) {
		Spider.create(new ThaiWaterPage3()).addUrl("http://www.thaiwater.net/DATA/REPORT/php/scada.php?lang=en")
				// .setDownloader(phantomDownloader)
				// .addPipeline(new SSTruyenDbPipeline()).setScheduler(new PriorityScheduler())
				.thread(2).run();
	}

	private List<Selectable> getDataTables(Page page) {
		List<Selectable> tables = null;
		Html html = page.getHtml();
		String url = page.getUrl().get();
		if (url.contains("chaopaya")) {
			tables = html.css("body>table>tbody>tr>td[valign=top]>table").nodes();
		} else if (url.contains("lampao")) {
			tables = html.css("body>table>tbody>tr>td[valign=top]>table").nodes();
			tables.remove(5);
			tables.remove(6);
		} else if (url.contains("rid_pk1")) {
			tables = html.css("body > table > tbody > tr:nth-child(2) > td>table").nodes();
			tables.remove(6);
		} else if (url.contains("phet_scada")) {
			tables = html.css("#table_main > tbody > tr > td>table").nodes();

		} else if (url.contains("mun_scada")) {
			tables = html.css("body > table > tbody > tr:nth-child(2) > td>table").nodes();
			tables.remove(7);

		} else if (url.contains("chumporn")) {
			tables = html.css("table[cellpadding=\"0\"]").nodes();
			tables.remove(5);
			tables.remove(5);

		}

		
		return tables;

	}

	private String getNameFromTbl(Selectable table) {
		String name = table.css("span.style_big_bu", "alltext").get();
		return name;
	}

	private List<Selectable> getTrs(String url, Selectable table) {
		List<Selectable> trs = null;
		if (url.contains("chaopaya")) {
			trs = table.css("tr>td>div>table>tbody>tr>td>table>tbody>tr").nodes();
		} else if (url.contains("lampao")) {
			trs = table.css("table>tbody>tr").nodes();
			trs.remove(0);
		} else if (url.contains("rid_pk1")) {
			trs = table.css("table>tbody>tr").nodes();
			trs.remove(0);
		} else if (url.contains("mun_scada")) {
			trs = table.css("table>tbody>tr").nodes();
			trs.remove(0);
		} else if (url.contains("chumporn")) {
			trs = table.css("table>tbody>tr").nodes();
			trs.remove(0);
		}

		return trs;
	}

	private String getTime(String url, List<Selectable> trs) {
		String time = null;
		int count = 0;
		Selectable tr = null;
		if (url.contains("chaopaya")) {
			tr = trs.get(0);
		} else if (url.contains("lampao")) {
			tr = trs.get(1);
		} else if (url.contains("rid_pk1")) {
			tr = trs.get(0);
		} else if (url.contains("mun_scada")) {
			tr = trs.get(0);
		} else if (url.contains("chumporn")) {
			tr = trs.get(0);
		}
		time = tr.css("td:last-child", "alltext").get();
		time = StringUtils.substringAfter(time, " ");
		time = time == null ? time : time.trim();
		return time;

	}

	private List<Selectable> cleanTrs(String url, List<Selectable> trs) {
		if (url.contains("chaopaya")) {
			trs.remove(0);
		} else if (url.contains("lampao")) {
			trs.remove(0);

		}
		return trs;

	}

	private List<Factor> getFactors(Page page) {
		List<Factor> factors = new ArrayList<>();
		List<Selectable> tables = null;
		Html html = page.getHtml();
		String url = page.getUrl().get();
		tables = getDataTables(page);
		for (Selectable table : tables) {
			String stationName = getNameFromTbl(table);
			List<Selectable> trs = getTrs(url, table);
			int trCount = 0;
			Factor factor = null;
			String time = getTime(url, trs);
			trs = cleanTrs(url, trs);
			for (Selectable tr : trs) {
				factor = new Factor();
				List<Selectable> tds = tr.css("td", "alltext").nodes();
				if (tds.size() == 3) {
					factor.setName(tds.get(0).get());
					factor.setValue(tds.get(1).get());
					factor.setUnit(tds.get(2).get());
					factor.setTime(time);
					if (factor != null) {
						factors.add(factor);
					}
				} else if (tds.size() == 5) {
					factor.setName(tds.get(0).get());
					factor.setUnit(tds.get(1).get());
					factor.setValue(tds.get(2).get());
					factor.setTime(time);
					if (factor != null) {
						factors.add(factor);
					}
					factor = new Factor();
					factor.setName(tds.get(3).get());
					factor.setUnit(tds.get(1).get());
					factor.setValue(tds.get(4).get());
					factor.setTime(time);
					if (factor != null) {
						factors.add(factor);
					}
				}
			}
			if (factor != null) {
				factors.add(factor);
			}

		}
		return factors;
	}

	public void process(Page page) {
		Html html = page.getHtml();
		List<Selectable> selects = html.css(".style_big_bu").nodes();
		int type = 0;
		Request prevReq = page.getRequest();
		type = prevReq.getExtra("type") == null ? 0 : Integer.parseInt(prevReq.getExtra("type").toString());
		switch (type) {
		case 0:
			int count = 0;
			for (Selectable selectable : selects) {
				/*
				 * if (count<2) { count++; continue; }
				 */
				String value = selectable.get();
				String url = StringUtils.substringBetween(value, "open_graph(\'", "\')");
				// System.out.println(url);
				// System.out.println(selectable.get());
				String title = selectable.css("a", "alltext").get();
				// System.out.println(title);

				if (url != null) {
					// lampao,chaopaya,rid_pk1,phet_scada,mun_scada
					if (url.contains("chumporn") == false) {// lampao,rid_pk1,mun_scada,chaopaya
						continue;
					}
					url = ROOT_URL + url;
					Request request = new Request(url);
					request.putExtra("type", 1);
					page.addTargetRequest(request);
					if (isDev == true) {
						break;
					}

				}
			}
			System.out.println("dfdfd");
			break;
		case 1:
			List<Selectable> tables = null;
			String url = page.getUrl().get();
			List<Factor> factors = new ArrayList<>();
			factors = getFactors(page);
			Gson gson = new Gson();
			System.out.println(gson.toJson(factors));
			break;

		default:
			break;
		}

	}

}
