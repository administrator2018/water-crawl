package com.crawl.water.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.crawl.water.common.Constants;
import com.crawl.water.dao.AreaDao;
import com.crawl.water.dao.CommonDao;
import com.crawl.water.dao.FactorDao;
import com.crawl.water.dao.StationDao;
import com.crawl.water.model.Area;
import com.crawl.water.model.Factor;
import com.crawl.water.model.Station;
import com.crawl.water.model.WaterInfo;
import com.google.gson.Gson;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

public class ThaiWaterPage1 implements PageProcessor {
	private Site site = Site.me().setCycleRetryTimes(3).setRetryTimes(3).setSleepTime(500).setTimeOut(1 * 60 * 1000)
			.setUserAgent(Constants.USER_AGENT)
			// .addHeader("Accept",
			// "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
			// .addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
			.setCharset("UTF-8");
	private String ROOT_URL = "http://www.thaiwater.net/DATA/site/content/fs_show_file.php?geo_code=02&type=province";
	private boolean isDev = true;
	private static final Logger logger = LogManager.getLogger(ThaiWaterPage1.class);
	AreaDao areaDao;
	StationDao stationDao;
	FactorDao factorDao;
	CommonDao commonDao;

	public ThaiWaterPage1() {
		areaDao = new AreaDao();
		stationDao = new StationDao();
		factorDao = new FactorDao();
		commonDao = new CommonDao();
	}

	@Override
	public Site getSite() {
		return site;
	}

	public static void main(String[] args) {
		CommonDao.clearDb();
		Spider.create(new ThaiWaterPage1())
				.addUrl("http://www.thaiwater.net/DATA/site/content/fs_show_file.php?geo_code=02&type=province")
				// .addPipeline(new SSTruyenDbPipeline()).setScheduler(new PriorityScheduler())
				.thread(2).run();
	}

	public void process(Page page) {
		
		Html html = page.getHtml();
		List<Selectable> trs = html.css("body > center > table > tbody > tr").nodes();
		int count = 0;
		String lastArea = "";

		List<WaterInfo> waterInfos = new ArrayList<>();
		for (Selectable tr : trs) {
			if (count < 2) {
				count++;
				continue;
			}
			if (tr.css("td:nth-child(2)", "alltext").get().length() == 1) {
				lastArea = tr.css("td:nth-child(1)", "alltext").get();
			} else {
				WaterInfo waterInfo = new WaterInfo();
				waterInfo.setArea(lastArea);
				waterInfo.setName(tr.css("td:nth-child(1)", "alltext").get());
				waterInfo.setDate(tr.css("td:nth-child(2)", "alltext").get());
				waterInfo.setTime(tr.css("td:nth-child(3)", "alltext").get());
				waterInfo.setSolar(tr.css("td:nth-child(4)", "alltext").get());
				waterInfo.setTemp(tr.css("td:nth-child(5)", "alltext").get());
				waterInfo.setHum(tr.css("td:nth-child(6)", "alltext").get());
				waterInfo.setPres(tr.css("td:nth-child(7)", "alltext").get());
				waterInfo.setRain(tr.css("td:nth-child(8)", "alltext").get());
				waterInfo.setRainToday(tr.css("td:nth-child(9)", "alltext").get());
				waterInfos.add(waterInfo);
				Gson gson = new Gson();
				System.out.println(gson.toJson(waterInfo));
				page.putField(Constants.DATA, waterInfo);
			}
		}

		for (WaterInfo waterInfo : waterInfos) {
			Area area = new Area();
			area.setOriginalName(waterInfo.getArea());
			areaDao.save(area);

			Station station = new Station();
			int areaId = areaDao.getIdByName(waterInfo.getArea());
			station.setOriginalName(waterInfo.getName());
			station.setAreaId(areaId);
			stationDao.save(station);

			int stationId = stationDao.getIdByName(waterInfo.getName());

			Factor factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(waterInfo.getDate());
			factor.setTime(waterInfo.getTime());
			factor.setName("Solar");
			factor.setUnit("W/m2");
			factor.setValue(waterInfo.getSolar());
			factorDao.save(factor);

			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(waterInfo.getDate());
			factor.setTime(waterInfo.getTime());
			factor.setName("Temp");
			factor.setUnit("C");
			factor.setValue(waterInfo.getTemp());
			factorDao.save(factor);

			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(waterInfo.getDate());
			factor.setTime(waterInfo.getTime());
			factor.setName("Hum");
			factor.setUnit("%RH");
			factor.setValue(waterInfo.getHum());
			factorDao.save(factor);

			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(waterInfo.getDate());
			factor.setTime(waterInfo.getTime());
			factor.setName("Pres.");
			factor.setUnit("mBar");
			factor.setValue(waterInfo.getPres());
			factorDao.save(factor);

			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(waterInfo.getDate());
			factor.setTime(waterInfo.getTime());
			factor.setName("Rain1Hr");
			factor.setUnit("mm");
			factor.setValue(waterInfo.getRain());
			factorDao.save(factor);

			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(waterInfo.getDate());
			factor.setTime(waterInfo.getTime());
			factor.setName("RainToda");
			factor.setUnit("mm");
			factor.setValue(waterInfo.getRainToday());
			factorDao.save(factor);
		}

	}

}
