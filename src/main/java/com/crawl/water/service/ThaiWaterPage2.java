package com.crawl.water.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.crawl.water.common.Constants;
import com.crawl.water.dao.AreaDao;
import com.crawl.water.dao.CommonDao;
import com.crawl.water.dao.FactorDao;
import com.crawl.water.dao.StationDao;
import com.crawl.water.model.Area;
import com.crawl.water.model.Factor;
import com.crawl.water.model.Station;
import com.crawl.water.model.ThaiWater2;
import com.google.gson.Gson;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class ThaiWaterPage2 implements PageProcessor {
	private Site site = Site.me().setCycleRetryTimes(3).setRetryTimes(3).setSleepTime(500).setTimeOut(1 * 60 * 1000)
			.setUserAgent(Constants.USER_AGENT)
			// .addHeader("Accept",
			// "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
			// .addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
			.setCharset("UTF-8");
	private String ROOT_URL = "http://www.thaiwater.net/v3/telemetering/wl/warning";
	private boolean isDev = true;
	private static final Logger logger = LogManager.getLogger(ThaiWaterPage2.class);
	AreaDao areaDao;
	StationDao stationDao;
	FactorDao factorDao;
	CommonDao commonDao;

	public ThaiWaterPage2() {
		areaDao = new AreaDao();
		stationDao = new StationDao();
		factorDao = new FactorDao();
		commonDao = new CommonDao();
	}
	@Override
	public Site getSite() {
		return site;
	}

	public static void main(String[] args) {
		CommonDao.clearDb();
		Spider.create(new ThaiWaterPage2())
				.addUrl("http://www.thaiwater.net/v3/json/telemetering/wl/warning")
				.thread(2).run();
	}

	public void process(Page page) {
		String data = page.getJson().toString();
		Gson gson = new Gson();
		
		ThaiWater2[] listData = gson.fromJson(data, ThaiWater2[].class);
		for (ThaiWater2 info : listData) {
			
			Area area = new Area();
			area.setOriginalName(info.getBasin());
			areaDao.save(area);

			Station station = new Station();
			int areaId = areaDao.getIdByName(area.getOriginalName());
			station.setCode(info.getCode());
			String sattionName = info.getName()+"ต."+ info.getTambon_name()+"ต."+info.getAmphoe_name()+"ต."+info.getProvince_name(); 
			station.setOriginalName(sattionName);
			station.setAreaId(areaId);
			stationDao.save(station);

			int stationId = stationDao.getIdByName(station.getOriginalName());

			Factor factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(info.getDate());
			factor.setTime(info.getTime());
			factor.setName("Water level");
			factor.setUnit("MRT");
			factor.setValue(info.getWl_present());
			factorDao.save(factor);
			
			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(info.getDate());
			factor.setTime(info.getTime());
			factor.setName("Bank of Thailand");
			factor.setUnit("");
			factor.setValue(info.getBank());
			factorDao.save(factor);		
			
			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(info.getDate());
			factor.setTime(info.getTime());
			factor.setName("Rain today");
			factor.setUnit("mm");
			factor.setValue(info.getRaintoday());
			factorDao.save(factor);	
			
			factor = new Factor();
			factor.setStationId(stationId);
			factor.setDate(info.getDate());
			factor.setTime(info.getTime());
			factor.setName("Water situation");
			factor.setUnit("");
			factor.setValue(info.getRain_yesterday());
			factorDao.save(factor);	
			
		}
		System.out.println(data);
	}

}
