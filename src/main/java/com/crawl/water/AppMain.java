package com.crawl.water;

import com.crawl.water.dao.CommonDao;
import com.crawl.water.service.ThaiWaterPage1;
import com.crawl.water.service.ThaiWaterPage2;

import us.codecraft.webmagic.Spider;

public class AppMain {

	public static void main(String[] args) {
		int control = Integer.parseInt(args[0]);
		CommonDao.clearDb();
		switch (control) {
		case 1:
			Spider.create(new ThaiWaterPage1())
					.addUrl("http://www.thaiwater.net/DATA/site/content/fs_show_file.php?geo_code=02&type=province")
					.thread(2).run();
			break;
		case 2:
			Spider.create(new ThaiWaterPage2())
			.addUrl("http://www.thaiwater.net/v3/json/telemetering/wl/warning")
			.thread(2).run();
			break;
		default:
			System.out.println("Tham so chi duoc 1(lay site 1) hoac 2(lay site 2)");
		}

	}

}
