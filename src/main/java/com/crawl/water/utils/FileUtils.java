package com.crawl.water.utils;
/**
 * 
 */

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author thi
 *
 */
public class FileUtils {

	public static void createFile(String content, String pathStr) {
		// Get the file reference
		Path path = Paths.get(pathStr);

		// Use try-with-resource to get auto-closeable writer instance
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			writer.write(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static String getSettingFile(String name, ClassLoader classLoader) {
		String result = null;
		result = classLoader.getResource(name + ".properties").getFile();
		return result;
	}

}
