INSERT INTO `water`.`station`
(`id`,
`code`,
`OriginalName`,
`TranslateName`,
`AreaId`)
VALUES
(<{id: }>,
<{code: }>,
<{OriginalName: }>,
<{TranslateName: }>,
<{AreaId: }>);
CREATE TABLE `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `originalName` varchar(200) DEFAULT NULL,
  `translateName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `factor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `time` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `stationId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=761 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `station` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `OriginalName` varchar(200) DEFAULT NULL,
  `TranslateName` varchar(200) DEFAULT NULL,
  `AreaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
