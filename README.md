1.Install java 8
 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
2.Install Mysql 
https://cdn.mysql.com//Downloads/MySQLInstaller/mysql-installer-community-8.0.11.0.msi

Note: tạo user, mật khẩu: root/123qwe

3.Create DB 
- Truy cập Mysql Workbench 
- Tạo DB tên water

4. Tải Eclipse J2SE
http://ftp.jaist.ac.jp/pub/eclipse/oomph/epp/photon/R/eclipse-inst-win64.exe
5. Import maven project

6. Đóng gói
- mvn clean compile assembly:single
7. Chạy AppMain
- Toi thu muc chua goi jar
C:\myrepos\water-crawl\src\main\resources
- Lấy dữ liệu site 1
java -jar water-crawl 1
- Lấy dữ liệu site 2
java -jar water-crawl 2
